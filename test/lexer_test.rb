# frozen_string_literal: true

require 'test_helper'

class LexerTest < Minitest::Test
  def lex(text, format = :DEFAULT, **options)
    fmt = Combustive::Formats.get(format).derive(options)
    @lexer = Combustive::Lexer.new(StringIO.new(text), fmt)
    @lexer.each.to_a
  end

  def t(*args)
    Combustive::Token.new(*args)
  end

  def comment(*args)
    Combustive::Comment.new(*args)
  end

  def eor
    Combustive::EndOfRecord.instance
  end

  def eof
    Combustive::EOF.instance
  end

  def test_unquoted
    assert_equal [t("Lorem"), eor, eof], lex(%(Lorem))
  end

  def test_quoted
    assert_equal [t("Lorem"), eor, eof], lex(%("Lorem"))
  end

  def test_unquoted_escaped
    assert_equal [t("Lore\tm"), eor, eof], lex(%(Lore\\tm), escape_char: "\\")
  end

  def test_quoted_escaped
    assert_equal [t("Lore\tm"), eor, eof], lex(%("Lore\\tm"), escape_char: "\\")
  end

  def test_quoted_doubles
    assert_equal [t('Lore"m'), eor, eof], lex(%("Lore""m"))
  end

  def test_unquoted_quoted
    assert_equal [t("foo"), t("bar"), eor, eof], lex(%(foo,"bar"))
  end

  def text_unquoted_with_spaces
    assert_equal [t("Lorem ipsum"), t("Dolor"), eor, eof], lex(%(Lorem ipsum,dolor))
  end

  def test_eat_spaces_till_delimiter
    assert_equal [t("Lorem ipsum"), t("Dolor"), eor, eof], lex(%("Lorem ipsum"         ,Dolor))
  end

  def test_eat_trailing_spaces
    assert_equal [t("Lorem ipsum"), t("Dolor"), eor, eof], lex(%(Lorem ipsum,"Dolor"  ))
  end

  def test_comment
    assert_equal [comment("Lorem ipsum dolor"), eof], lex(%(#Lorem ipsum dolor), comment_start: '#')
  end

  def test_blank_fields
    assert_equal [t(""), t("Lorem"), t(""), eor, eof], lex(%(,Lorem,))
  end

  def test_multiple_lines
    assert_equal [t("Lorem"), t("Ipsum"), eor,
                  t("Dolor"), eor,
                  eof],
                 lex(%(Lorem,Ipsum\r\nDolor))
  end

  def test_alternative_record_separator
    assert_equal [t("Lorem"), t("Ipsum"), eor,
                  t("Dolor"), eor,
                  eof],
                 lex(%(Lorem,Ipsum\x00Dolor), record_separator: "\x00")
  end
end
