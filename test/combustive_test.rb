# frozen_string_literal: true

require "test_helper"

class CombustiveTest < Minitest::Test
  def test_that_it_has_a_version_number
    refute_nil ::Combustive::VERSION
  end

  def test_file_parsers
    files = Dir.glob(File.expand_path("resources/**/test_*.txt", __dir__))
    files.each do |test_file|
      fp = File.open(test_file)
      csv_file, *options = fp.readline.split
      expected_repr = fp.readline
      parser = Combustive::Parser.new(csv_file, parse_options(options))
      assert_equal(expected_repr, parser.to_s)
    end
  end

  private

  def parse_options(options)
    options.map do |opt|
      name, _, value = opt.partition("=")
      [underscored(name), value_of(value)]
    end.to_h
  end

  def value_of(value)
    case value
    when "true" then true
    when nil then true
    when "false" then false
    else value
    end
  end

  def underscored(name)
    name.gsub(/([[:lower:]])([[:upper:]])/, '\1_\2').downcase.to_sym
  end
end
