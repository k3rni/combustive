# frozen-string-literal: true

require 'test_helper'

class CommonsCSVTest < Minitest::Test
  def self.list_test_files
    Dir.glob(File.expand_path("resources/**/test*.txt", __dir__))
  end

  list_test_files.each do |filename|
    test_name = "test_#{File.basename(filename, '.*').gsub(/^test_?/, '')}"
    define_method(test_name) do
      run_test_file(filename)
    end
  end

  private

  def run_test_file(filename)
    File.open(filename) do |fp|
      dir = File.dirname(fp)
      csv_file, _, options = fp.readline.partition(' ')
      format = Combustive::Formats::DEFAULT.derive(delimiter: ",", quote_char: '"', **format_options(options))
      check_comments = options[/CheckComments/]
      format_desc = fp.readline
      assert_format_matches format_desc, format
      parser = Combustive::Parser.new(File.join(dir, csv_file.strip), format)
      verify_output(fp.readlines.each, parser, comments: check_comments)
    end
  end

  def format_options(options)
    options.split.map do |opt|
      case opt
      when /^IgnoreEmpty=(true|false)/
        { ignore_empty_lines: ($1 == "true") }
      when /^IgnoreSpaces=(true|false)/
        { ignore_surrounding_spaces: ($1 == "true") }
      when /^CommentStart=(.)/
        { comment_start: $1 }
      else
        {}
      end
    end.reduce({}, &:merge)
  end

  def assert_format_matches(description, format)
    description.split.each do |opt|
      case opt
      when /^Delimiter=<(.)>/
        assert_equal $1, format.delimiter
      when /^QuoteChar=<(.)>/
        assert_equal $1, format.quote_char
      when /^CommentStart=<(.)>/
        assert_equal $1, format.comment_start
      when "EmptyLines:ignored" # NOTE: No other value appears in testcases
        assert format.ignore_empty_lines
      when "SkipHeaderRecord:false"
        refute format.skip_header_record
      when "SurroundingSpaces:ignored"
        assert format.ignore_surrounding_spaces
      end
    end
  end

  def verify_output(lines, parser, comments: false)
    parser.each do |actual|
      parsed = repr_row(actual)
      parsed += '#' + actual.comment.gsub("\n", "\\n") if comments

      count = actual.size
      expected = read_test_data(lines)
      assert_equal expected.strip.gsub("\n", "\\n"), "#{count}:#{parsed}"
    end
  end

  def repr_row(items)
    return "" if items.empty?
    ['[', *items.map(&:to_s).join(', '), ']'].join('')
  end

  def read_test_data(lines)
    loop do
      line = lines.next
      return line if line && !line&.start_with?('#')
    end
    ''
  end
end
