# frozen_string_literal: true

require 'test_helper'

class PrinterTest < MiniTest::Test
  def setup
    @out = StringIO.new
  end

  def output
    @out.string
  end

  def printer(format, **options)
    fmt = Combustive::Formats.get(format).derive(options)
    Combustive::Printer.new(@out, fmt)
  end

  def printer_with_comments(format, now)
    fmt = Combustive::Formats.const_get(format).derive(
      header_comments: ["Generated with Combustive", now],
      comment_start: "#",
      header: %w[Col1 Col2]
    )
    Combustive::Printer.new(@out, fmt).tap do |pr|
      pr << %w[A B]
      pr << %w[C D]
    end
  end

  def test_delimiter_quoted
    pr = printer(:DEFAULT, quote_char: '\'')
    pr.write "a,b,c"
    pr.write "xyz"
    assert_equal "'a,b,c',xyz", output
  end

  def test_delimiter_quote_none
    pr = printer(:DEFAULT, escape_char: "!", quote_mode: :none)
    pr.write "a,b,c"
    pr.write "xyz"
    assert_equal "a!,b!,c,xyz", output
  end

  def test_delimiter_escaped
    pr = printer(:DEFAULT, escape_char: "!", quote_char: nil)
    pr.write "a,b,c"
    pr.write "xyz"
    assert_equal "a!,b!,c,xyz", output
  end

  def test_delimiter_plain
    pr = printer(:DEFAULT, quote_char: nil)
    pr.write "a,b,c"
    pr.write "xyz"
    assert_equal "a,b,c,xyz", output
  end

  def test_disabled_comment
    pr = printer(:DEFAULT)
    pr.write_comment "this is a comment"
    assert_equal "", output
  end

  def test_eol_escaped
    pr = printer(:DEFAULT, quote_char: nil, escape_char: "!")
    pr.write "a\rb\nc"
    pr.write "x\fy\bz"
    assert_equal "a!rb!nc,x\fy\bz", output
  end

  def test_eol_plain
    pr = printer(:DEFAULT, quote_char: nil)
    pr.write "a\rb\nc"
    pr.write "x\by\fz"
    assert_equal "a\rb\nc,x\by\fz", output
  end

  def test_eol_quoted
    pr = printer(:DEFAULT, quote_char: "'")
    pr.write "a\rb\nc"
    pr.write "x\by\fz"
    assert_equal "'a\rb\nc',x\by\fz", output
  end

  def test_escape_backslash_1
    pr = printer(:DEFAULT, quote_char: "'")
    pr.write("\\")
    assert_equal "\\", output
  end

  def test_escape_backslash_2
    pr = printer(:DEFAULT, quote_char: "'")
    pr.write("\\\r")
    assert_equal "'\\\r'", output
  end

  def test_escape_backslash_3
    pr = printer(:DEFAULT, quote_char: "'")
    pr.write("X\\\r")
    assert_equal "'X\\\r'", output
  end

  def test_escape_backslash_4
    pr = printer(:DEFAULT, quote_char: "'")
    pr.write("\\\\")
    assert_equal "\\\\", output
  end

  def test_escape_null_1
    pr = printer(:DEFAULT, escape_char: nil)
    pr.write("\\")
    assert_equal "\\", output
  end

  def test_escape_null_2
    pr = printer(:DEFAULT, escape_char: nil)
    pr.write("\\\r")
    assert_equal %("\\\r"), output
  end

  def test_escape_null_3
    pr = printer(:DEFAULT, escape_char: nil)
    pr.write("X\\\r")
    assert_equal %("X\\\r"), output
  end

  def test_escape_null_4
    pr = printer(:DEFAULT, escape_char: nil)
    pr.write("\\\\")
    assert_equal "\\\\", output
  end

  def test_excel_printer_1
    pr = printer(:EXCEL)
    pr << %w[a b]
    assert_equal "a,b\r\n", output
  end

  def test_excel_printer_2
    pr = printer(:EXCEL)
    pr << ["a,b", "b"]
    assert_equal %("a,b",b\r\n), output
  end

  def test_header
    pr = printer(:DEFAULT, quote_char: nil, header: %w[C1 C2 C3])
    pr << %w[a b c]
    pr << %w[x y z]
    assert_equal %(C1,C2,C3\r\na,b,c\r\nx,y,z\r\n), output
  end

  def test_header_comment_excel
    now = Time.now
    printer_with_comments(:EXCEL, now)
    assert_equal %(# Generated with Combustive\r\n# #{now}\r\nCol1,Col2\r\nA,B\r\nC,D\r\n), output
  end

  def test_header_not_set
    pr = printer(:DEFAULT, quote_char: nil)
    pr << %w[a b c]
    pr << %w[x y z]
    assert_equal "a,b,c\r\nx,y,z\r\n", output
  end

  def test_invalid_format
    assert_raises(ArgumentError) do
      printer(:DEFAULT, delimiter: "\r")
    end
  end

  def test_jira135_part1
    pr = printer(:DEFAULT, record_separator: "\n", quote_char: '"', escape_char: '\\')
    pr << ['"']
    assert_equal "\"\\\"\"\n", output
  end

  def test_jira135_part2
    pr = printer(:DEFAULT, record_separator: "\n", quote_char: '"', escape_char: '\\')
    pr << ["\n"]
    assert_equal '"\\n"\n', output
  end

  def test_jira135_part3
    pr = printer(:DEFAULT, record_separator: "\n", quote_char: '"', escape_char: '\\')
    pr << ["\\"]
    assert_equal '"\\"', output
  end

  def test_jira135_all
    pr = printer(:DEFAULT, record_separator: "\n", quote_char: '"', escape_char: '\\')
    pr << ['"']
    pr << ["\n"]
    pr << ["\\"]
    assert_equal "\"\\\"\",\"\\n\",\"\\\"\n", output
  end
end
