# frozen_string_literal: true

require "simplecov"

SimpleCov.start
$LOAD_PATH.unshift File.expand_path('../lib', __dir__)
require "combustive"

require "minitest/autorun"
require "byebug"

class Combustive::Comment
  def inspect
    %(<##{content}#>)
  end
end

class Combustive::Token
  def inspect
    %(<#{content}>)
  end
end

class Combustive::EOF
  def inspect
    %(EOF)
  end
end

class Combustive::EndOfRecord
  def inspect
    %(EOR)
  end
end
