# frozen_string_literal: true

require "combustive/version"
require "combustive/formats"
require "combustive/parser"
require "combustive/printer"

module Combustive
  DEFAULT = Formats::DEFAULT
  EXCEL = Formats::EXCEL
  RFC4180 = Formats::RFC4180

  def self.resolve_format(format_or_symbol)
    return format_or_symbol if format_or_symbol.is_a?(Formats::Format)
    const_get(format_or_symbol.upcase)
  rescue NameError
    known = %i[default excel rfc4180]
    raise ArgumentError, "Unknown format `#{format_or_symbol}`. " \
                         "Pass a known format name (#{known}) or an instance of Format."
  end
end
