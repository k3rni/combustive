# frozen_string_literal: true

require 'singleton'
require 'forwardable'

module Combustive
  # Those are singletons for ease of comparison and testing.
  class EOF; include Singleton; end
  class EndOfRecord; include Singleton; end
  class Invalid; include Singleton; end
  Comment = Struct.new(:content)
  Token = Struct.new(:content)

  class Lexer
    extend Forwardable
    def_delegators :@format, :ignore_empty_lines, :comment_start, :ignore_surrounding_spaces,
                   :delimiter, :quote_char, :escape_char, :record_separator

    def initialize(io, format)
      @io = io
      @format = format
      @unget_buf = []
    end


    def each
      return enum_for(:each) unless block_given?

      comment_text = []
      @io.each(record_separator) do |line|
        puts line
        break if line.nil?
        next if line.empty? && ignore_blanks
        if comment_start && line.start_with?(comment_start)
          comment_text << line[1..-1].strip
          next
        elsif comment_text.size > 0
          yield Comment.new(comment_text.join("\n"))
          comment_text.clear
        end
        line.lstrip! if ignore_surrounding_spaces
        parse_line(line.each_char) { |token| yield token }
      end
      yield eof!
    end

    def parse_line(input)
      token = nil
      eol = nil
      loop do
        case (ch = read(input))
        when delimiter
          yield Token.new("")
        when quote_char
          token, eol = parse_encapsulated_token(input)
          yield token
        else
          unget(ch)
          token, eol = parse_simple_token(input)
          yield token
        end
      end
      yield Token.new("") unless eol
      yield eor!
    end

    def parse_simple_token(input)
      content = +''
      eol = true
      loop do
        case ch = read(input)
        when "\r", "\n"
          # Silently eat trailing newline
        when record_separator # Hit end of record
          break
        when delimiter
          eol = false
          break
        when escape_char
          content << unescaped(input.next)
        else
          content << ch
        end
      end

      [Token.new(content), eol]
    end

    def parse_encapsulated_token(input)
      content = +''
      loop do
        case (ch = input.next)
        when escape_char
          content << unescaped(input.next)
        when quote_char
          begin
            if input.peek == quote_char # it's a doubled quotechar, just eat it
              content << input.next
            else # closing quotechar
              loop do # Eat whitespace till delimiter
                ch = input.next
                return [Token.new(content), false] if ch == delimiter
                raise ArgumentError, "non-whitespace `#{ch}` betweend closing quote and delim" unless whitespace?(ch)
              end
            end
          rescue StopIteration # EOL when searching for endquote
            break
          end
        else # any other char
          content << ch
        end
      end
      [Token.new(content), true]
    end

    def read(input)
      @unget_buf.shift || input.next
    end

    def unget(char)
      @unget_buf.unshift(char)
    end

    # Determines if the specified character (Unicode code point) is white space according to Java. A character is a Java whitespace character if and only if it satisfies one of the following criteria:
    # It is a Unicode space character (SPACE_SEPARATOR, LINE_SEPARATOR, or PARAGRAPH_SEPARATOR) but is not also a non-breaking space ('\u00A0', '\u2007', '\u202F').
    # It is '\t', U+0009 HORIZONTAL TABULATION.
    # It is '\n', U+000A LINE FEED.
    # It is '\u000B', U+000B VERTICAL TABULATION.
    # It is '\f', U+000C FORM FEED.
    # It is '\r', U+000D CARRIAGE RETURN.
    # It is '\u001C', U+001C FILE SEPARATOR.
    # It is '\u001D', U+001D GROUP SEPARATOR.
    # It is '\u001E', U+001E RECORD SEPARATOR.
    # It is '\u001F', U+001F UNIT SEPARATOR.
    def whitespace?(char)
      %W[\t \n \f \r \x20
         \xA0 \u1680 \u2000 \u2001 \u2002 \u2003 \u2004 \u2005 \u2006 \u2007 \u2008 \u2009
         \u200A \u202F \u205F \u3000 \u2028 \u2029
         \u000B \u001C \u001D \u001E \u001F].include?(char)
    end

    def unescaped(char)
      { 'n' => "\n", 'r' => "\r",
        'f' => "\f", 'b' => "\b",
        't' => "\t" }.fetch(char, char)
    end

    def eof!
      EOF.instance
    end

    def eor!
      EndOfRecord.instance
    end
  end
end
