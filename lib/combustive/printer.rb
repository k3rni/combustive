# frozen_string_literal: true

module Combustive
  class Printer
    def initialize(filename_or_io, format, **options)
      @out = if filename_or_io.respond_to? :write
               filename_or_io
             else
               File.open(filename_or_io, 'wb'**options)
             end
      @format = format
      @new_record = true

      format.header_comments.to_a.each { |comment| write_comment(comment) }
      self << format.header if format.header
    end

    attr_reader :format, :out

    def <<(items)
      # TODO: set and clear newRecord
      items.each { |val| write(val) }
      writeln
      self
    end

    def write(val)
      out.write(format.print(val, @new_record))
      @new_record = false
    end

    def writeln
      out.write(format.record_separator)
      @new_record = true
    end

    def write_comment(comment)
      cs = format.comment_start
      return unless cs
      writeln unless @new_record
      out.write(cs)
      out.write(" ")
      out.write(comment.to_s.gsub(/(\r?\n)/, "\\1#{cs} "))
      writeln
    end
  end
end
