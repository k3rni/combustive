# frozen_string_literal: true

require 'combustive/lexer'

module Combustive
  class Row < Array
    attr_accessor :comment

    def inspect
      [super, comment].join('')
    end
  end

  class Parser
    def initialize(path_or_io, format = nil, **options)
      @io = if path_or_io.respond_to? :write
              path_or_io
            else
              File.open(path_or_io, 'rb', **options)
            end
      @format ||= Combustive.resolve_format(format || options.fetch(:format, :default))
      @lexer = Lexer.new(@io, @format)
      @record_number = 0
      initialize_headers
    end

    def parse
      each.to_a
    end

    def each
      return enum_for(:each) unless block_given?
      row = Row.new
      lexer.each do |token|
        case token
        when EndOfRecord
          unless row.empty? && format.ignore_empty_lines
            yield row.freeze
            row = Row.new # Must be new array for each row
          end
        when EOF
          yield row unless row.empty?
          break
        when Comment
          row.comment = token.content
        when Token
          row << token.content
        else
          raise RuntimeError, "Unknown token #{token}"
        end
      end
    end

    attr_reader :format, :lexer, :io

    def initialize_headers
      # TODO
    end
  end
end
