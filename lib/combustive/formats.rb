# frozen_string_literal: true

require 'byebug'

module Combustive
  module Formats
    def self.get(format)
      const_get format
    end

    QUOTE_MODES = %i[all all_non_null non_numeric none minimal].freeze
    Format = Struct.new(
      :delimiter, # the char used for value separation, must not be a line break character
      :quote_char, # the Character used as value encapsulation marker, may be {@code null} to disable
      :quote_mode, # the quote mode
      :comment_start, # the Character used for comment identification, may be {@code null} to disable
      :escape_char, # the Character used to escape special characters in values, may be {@code null} to disable
      :ignore_surrounding_spaces, # {@code true} when whitespaces enclosing values should be ignored
      :ignore_empty_lines, # {@code true} when the parser should skip empty lines
      :record_separator, # the line separator to use for output
      :null_string, # the line separator to use for output
      :header_comments, # the comments to be printed by the Printer before the actual CSV data
      :header, # the header
      :skip_header_record, # TODO
      :allow_missing_column_names, # TODO
      :ignore_header_case, # TODO
      :trim, # TODO
      :trailing_delimiter, # TODO
      :auto_flush # nd
    ) do

      def derive(options)
        dup.tap do |format|
          options.each { |opt, value| format.send("#{opt}=", value) }
          format.send(:validate!) # because it's private
        end
      end

      def format(value)
        text = value.nil? ? represent_nil : value.to_s
        trim ? text.trim : text
      end

      def print(value, new_record = true)
        (new_record ? "" : delimiter) + quote_and_escape(value, new_record)
      end

      def represent_nil
        return "" if null_string.nil?
        quote_mode == :all ? "#{quote_char}#{null_string}#{quote_char}" : null_string
      end

      def quote(value, text, new_record)
        quoting = case quote_mode
                  when :all, :all_non_null then true
                  when :non_numeric then !(value.is_a? Numeric)
                  when :none then return escape(text)
                  when :minimal, nil then quotes_needed?(text, new_record)
                  end
        quoting ? "#{quote_char}#{text}#{quote_char}" : text
      end

      def escape(text)
        esc = escape_char
        text.gsub(escape_rx, delimiter => "#{esc}#{delimiter}", esc => "#{esc}#{esc}",
                             "\n" => "#{esc}n", "\r" => "#{esc}r")
      end

      def quote_and_escape(value, new_record)
        if quote_char
          quote(value, format(value), new_record)
        elsif escape_char
          escape(format(value))
        else
          format(value)
        end
      end

      def escape_rx
        @escape_rx ||= Regexp.union(*[delimiter, escape_char, "\n", "\r"].compact)
      end

      def quote_rx
        @quote_rx ||= Regexp.union(*[delimiter, quote_char, comment_start, "\n", "\r", " "].compact)
      end

      def quotes_needed?(text, new_record)
        (text.empty? && new_record) || text =~ quote_rx
      end

      private

      def validate!
        # rubocop:disable Metrics/LineLength
        raise ArgumentError, "delimiter cannot be a line break" if delimiter == "\r"
        raise ArgumentError, "quote character and delimiter cannot be the same" if quote_char && delimiter == quote_char
        raise ArgumentError, "escape character and delimiter cannot be the same" if escape_char && escape_char == delimiter
        # rubocop:enable Metrics/LineLength
        validate_comment!
        validate_header!
      end

      def validate_comment!
        # rubocop:disable Metrics/LineLength
        raise ArgumentError, "comment start character and delimiter cannot be the same" if comment_start && delimiter == comment_start
        raise ArgumentError, "comment start character and quote character cannot be the same" if comment_start && delimiter == quote_char
        raise ArgumentError, "comment start chararter and escape character cannot be the same" if comment_start && escape_char == comment_start
        raise ArgumentError, "no quotes mode set but no escape character defined" if escape_char.nil? && quote_mode == :none
        # rubocop:enable Metrics/LineLength
      end

      def validate_header!
        head = header.to_a
        return if head.empty?
        duplicates = head - head.uniq
        raise ArgumentError, "header contains duplicate columns #{duplicates}" unless duplicates.empty?
      end
    end

    DEFAULT = Format.new(',', '"', nil, nil, nil,
                         false, true, "\r\n", nil, nil,
                         nil, false, false, false, false,
                         false, false)
    EXCEL = DEFAULT.derive(ignore_empty_lines: true, allow_missing_column_names: true)
    MYSQL = DEFAULT.derive(delimiter: "\t", escape_char: "\\", quote_char: nil,
                           ignore_empty_lines: false, record_separator: "\n", null_string: "\\N",
                           quote_mode: :all_non_null)
    RFC4180 = DEFAULT.derive(ignore_empty_lines: false)
  end
end
